#!/bin/sh
# inspired by https://github.com/umiddelb/z2d/tree/master/pine64

display_usage() {
    echo "$0 </dev/mmcblk0> </mnt/SD>"
}

SD_DEV=$1
SD_MOUNT_POINT=$2

[ "x$1" == "x" ] && display_usage && exit
[ "x$2" == "x" ] && display_usage && exit

[[ "x$UID" != "x0" ]] && echo "run as root" && exit

if $(df | grep /$ | grep -q "^$SD_DEV"); then
    echo "Target and current system device are the same. Can't do that!"
    exit
fi

CWD=$(pwd)

SIMPLEIMAGE_VERSION=${SIMPLEIMAGE_VERSION:-"latest"}
LINUX_VERSION=${LINUX_VERSION:-"latest"}
SLACKWARE_VERSION=${SLACKWARE_VERSION:-"current"}
SLACKWARE_MINIROOTFS_VERSION=${SLACKWARE_MINIROOTFS_VERSION:-"30Aug18"}

SSH_PUB_KEY=${SSH_PUB_KEY:-~/.ssh/id_ed25519.pub}

echo "Going further will wipe ${SD_DEV} and you _will_ lose all data on it !"
echo "/!\\ LAST CHANCE TO HIT Ctrl-C /!\\"
read

wget -c https://www.stdin.xyz/downloads/people/longsleep/pine64-images/simpleimage-pine64-${SIMPLEIMAGE_VERSION}.img.xz
unxz simpleimage-pine64-${SIMPLEIMAGE_VERSION}.img.xz
dd if=simpleimage-pine64-${SIMPLEIMAGE_VERSION}.img | pv | dd of=$SD_DEV bs=1024

/bin/echo -e "d\n2\nn\np\n2\n143360\n\nw\n" | sudo fdisk $SD_DEV

sleep 3
partprobe

sudo mkfs.ext4 -F -O ^has_journal -O ^64bit -b 4096 -L "rootfs" ${SD_DEV}2

mount ${SD_DEV}2 $SD_MOUNT_POINT

mkdir -p $SD_MOUNT_POINT/bootenv
mount ${SD_DEV}1 $SD_MOUNT_POINT/bootenv

wget -c http://ftp.arm.slackware.com/slackwarearm/slackwarearm-devtools/minirootfs/roots/slack-${SLACKWARE_VERSION}-miniroot_${SLACKWARE_MINIROOTFS_VERSION}.tar.xz
tar --numeric-owner -C $SD_MOUNT_POINT/ -xpJf slack-${SLACKWARE_VERSION}-miniroot_${SLACKWARE_MINIROOTFS_VERSION}.tar.xz

wget -c https://www.stdin.xyz/downloads/people/longsleep/pine64-images/linux/linux-pine64-${LINUX_VERSION}.tar.xz
tar --numeric-owner -C $SD_MOUNT_POINT/ -xpJf linux-pine64-${LINUX_VERSION}.tar.xz
cp -R $SD_MOUNT_POINT/boot/* $SD_MOUNT_POINT/bootenv/

cat <<EOF > $SD_MOUNT_POINT/etc/modprobe.d/blacklist
8723bs_vq0
EOF

cat <<EOF >> $SD_MOUNT_POINT/etc/rc.d/rc.modprobe.local
/sbin/modprobe 8723bs
EOF

cat <<EOF >> $SD_MOUNT_POINT/etc/fstab

/dev/mmcblk0p1	/bootenv	vfat	defaults		1	1
/dev/mmcblk0p2	/		ext4	defaults,discard	1	1
tmpfs		/tmp		tmpfs	defaults,mode=777	0	0
EOF

cat <<EOF > $SD_MOUNT_POINT/etc/motd
 ____  _             __   _  _
|  _ \(_)_ __   ___ / /_ | || |
| |_) | | '_ \ / _ \ '_ \| || |_
|  __/| | | | |  __/ (_) |__   _|
|_|   |_|_| |_|\___|\___/   |_|
                               Slackware
EOF

cp $CWD/pine64-kernel.SlackBuild $SD_MOUNT_POINT/root

mkdir -p $SD_MOUNT_POINT/root/.ssh/
cp $SSH_PUB_KEY $SD_MOUNT_POINT/root/.ssh/authorized_keys

umount $SD_MOUNT_POINT/bootenv
umount $SD_MOUNT_POINT
sync

curl -s http://ftp.arm.slackware.com/slackwarearm/slackwarearm-devtools/minirootfs/roots/slack-${SLACKWARE_VERSION}-miniroot_details.txt | grep -C1 "User"

echo "All done, eject your microSD card, insert it in the Pine64 and boot it :)"
